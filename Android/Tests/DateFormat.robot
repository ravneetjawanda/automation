*** Settings ***
Library  DateTime

*** Keywords ***
Manipulate current time
    ${CurrentDate}=  Get Current Date  result_format=%Y-%m-%d %H:%M:%S.%f

    ${datetime} =	Convert Date  ${CurrentDate}  datetime
    Log  ${datetime.year}	
    Log	 ${datetime.month}
    Log	 ${datetime.day}	
    Log	 ${datetime.hour}
    Log	 ${datetime.minute}
    Log	 ${datetime.second}
    Log	 ${datetime.microsecond}

    ${newdatetime} =  Add Time To Date  ${CurrentDate}  2 days
    Log  ${newdatetime}
    ${newdatetime} =  Add Time To Date  ${CurrentDate}  2 hours
    Log  ${newdatetime}
    ${newdatetime} =  Add Time To Date  ${CurrentDate}  30 minutes
    Log  ${newdatetime}
    ${newdatetime} =  Add Time To Date  ${CurrentDate}  30 seconds
    Log  ${newdatetime}
    
	 ${hour}=   Get Current Date   result_format=%H
	 ${hour}=   Convert To Integer   ${hour}
	 ${min}=   Get Current Date   result_format=%M
	 ${min}=   Convert To Integer   ${min}
	 ${minmodified}=   Evaluate   ${min}+55
	 ${hourmodified}=   Evaluate   ${hour}+2
	 Log  ${hourmodified}
     Log  ${minmodified}