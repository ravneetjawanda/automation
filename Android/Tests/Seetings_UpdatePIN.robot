*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_Profile.robot
Resource    ../Common/Common_ManageCard.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Verify thats user is able to reset Thier card PIN
    [Documentation]    Test that users are able to change PIN
    [Tags]    Edit_Mobile 
     Enter Login Page
     Enter username    rav99@payfare.com
     Enter Password    Payfare@1
     Sign In
     Get verified using phone
     Click on Next button
     Sleep    1
     For Test    2
     For Test1   5
     Select Next
     Click on Drawer menu
     Select Manage Card
     Select Change PIN
     Enter verification password    Payfare@1
     Click Submit button
     Enter PIN    1234
     Confirm PIN    1234
     Hit Next button
     Verify confirmation message