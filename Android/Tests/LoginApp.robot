*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Suite Setup    Log    App login Test Suite Setup 
Suite Teardown    Log    App Login Test Suite Teardown
Test Setup    Open Application Test Setup
Test Teardown    Close Application Teardown
Default Tags    Smoke

*** Variables ***
@{Credentials}    rav99@payfare.com    Payfare@1
@{InvalidCrdenatials}    rkaur@payfare.com    Payfare123
&{LoginData}    Email=rav99@payfare.com    Password=Payfare@1

*** Test cases ***
ValidLogin
    [Documentation]    Test that users can login with valid password  
    [Tags]    Valid_Credentials
    Enter Login Page
    Enter username    @{Credentials}[0]
    Enter Password    @{Credentials}[1] 
    Sign In
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next  
    Wait Until Element Is Visible    id=dash_card_checking_balance_amount        
    
Invalid username And Valid Password 
    [Documentation]    Test that users can login with invalid username and password 
    [Tags]    Invalid_Credentials
    Enter Login Page
    Enter username    @{InvalidCrdenatials}[0]
        Enter Password    @{Credentials}[1]
    Sign In 
    Check that Login fails
    
Valid username And Invalid Password 
    [Documentation]    Test that users can login with valid username and invalid password 
    [Tags]    Invalid_Credentials
    Enter Login Page
    Enter username    @{Credentials}[0]
    Enter Password    @{InvalidCredentials}[1]
    Sign In 
    Check that Login fails
    
Invalid username And Invalid Password 
    [Documentation]    Test that users can login with invalid username and invalid password 
    [Tags]    Invalid_Credentials
    Enter Login Page
    Enter username    @{InvalidCredentials}[1]
    Enter Password    @{InvalidCredentials}[1]
    Sign In 
    Check that Login fails

