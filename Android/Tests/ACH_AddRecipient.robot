*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Test Setup    Open Application Test Setup
Test Teardown    Close Application Teardown

*** Test cases ***
Add a recipient for ACH
    [Documentation]    Test that users are able to add a recipient successfully  
    [Tags]    ACH
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In 
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next
    Enter Move money nav
    Enter Send money to someone page
    Enter Recipient screen
    Click on Add button
    Enter FirstName    Ravneet
    Enter LastName    Kaur
    Enter Mobile number    4161231234
    Enter bank name    CIBC
    Enter account number    123456
    Select Account Type
     :FOR    ${i}    IN RANGE    10
    \    Repeat Keyword    3 times   Swipe Up  
    \    Exit For Loop If    ${i} == 10
    \    Log    ${i}
    Log    Exited 
    Enter confirm account number    123456  
    Enter routing number    121000248
    Click on Save button
    