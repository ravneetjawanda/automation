*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Resource    ../Common/Common_Billpay.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Add a Custom payee for Bill pay
    [Documentation]    Test that users are able to add a recipient successfully  
    [Tags]    Adding_bill_payee
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next  
    Enter Move money nav
    Enter Bill pay page
    Enter Payee screen
    Click on Add Payee button
    Enter or select payee screen
    Select Manually Add payee button
    Enter manual payee name    Landlord
    Enter address    1451 Monte Diablo ave
    Enter city    San mateo
    Select state  
    Enter manual payee zipcode    94401
    Select Save button
    Click Confirm address button
    