*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_Profile.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Verify error message is being displayed when user enetr amount more than the available balance 
    [Documentation]    Test that users are able to edit phone
    [Tags]    Edit_Mobile 
     Enter Login Page
     Enter username    rav99@payfare.com
     Enter Password    Payfare@1
     Sign In
     Get verified using phone
     Click on Next button
     Sleep    1
     For Test    2
     For Test1   5
     Select Next
     Click on Drawer menu
     Select Settings
     Select Edit Profile
     Select Edit Address
     Enter verification password    Payfare@1
     Click Submit button
     Edit Street    1451 Monte diablo ave
     Hide Keyboard    
     Edit City    San mateo
     Hide Keyboard    
     Enter Zipcode    94401
     Hide Keyboard    
     Select State
     Select Save button
     Select Address as entered checkbox
     Click Confirm Address
     Verify Toast message
     