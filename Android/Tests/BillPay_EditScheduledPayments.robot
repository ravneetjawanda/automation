*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Resource    ../Common/Common_Billpay.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Add a Certified payee for Bill pay
    [Documentation]    Test that users are able to sechdule future dated payments  
    [Tags]    SendingMoney_bill_payee
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next 
    Enter Move money nav
    Select Scheduled payments 
    Click Edit Button
    Edit Amount    4.33
    Edit Date
    Click Save button