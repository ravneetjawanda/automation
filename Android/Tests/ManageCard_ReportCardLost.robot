*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_Profile.robot
Resource    ../Common/Common_ManageCard.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Verify user is able to mark their card as Lost
    [Documentation]    Test that users are able to report their card as lost
    [Tags]    Edit_Mobile 
     Enter Login Page
     Enter username    rav99@payfare.com
     Enter Password    Payfare@1
     Sign In
     Get verified using phone
     Click on Next button
     Sleep    1
     For Test    2
     For Test1   5
     Select Next
     Click on Drawer menu
     Select Manage Card
     Select Report a card issue
     Enter verification password    Payfare@1
     Click Submit button
     Select Reason
     Enter Description    Testing
     Select Next button
     Common_ManageCard.Edit Street    9207 Evelyn Ave
     Enter Apartment/Unit    Unit 351
     Common_ManageCard.Edit City    California City
     Common_ManageCard.Enter Zipcode    93505
     Select State
     Click Next
     Select Suggested address
     Click Confirm Address
     Common_ManageCard.Verify Toast message
     
     