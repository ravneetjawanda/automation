*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Test Setup    Open Application Test Setup
Test Teardown    Close Application Teardown
Default Tags    Smoke

*** Test cases ***
Add a recipient for ACH
    [Documentation]    Test that users are able to delete a recipient successfully  
    [Tags]    ACH
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In 
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next
    Enter Move money nav
    Enter Send money to someone page
    Enter Recipient screen
    Select Recipient
    Enter Amount    2.00
    Hide Keyboard
    Click Send Money button
    Click Confirm button
    Verify Amount