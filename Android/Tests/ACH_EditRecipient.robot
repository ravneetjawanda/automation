*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Test Setup    Open Application Test Setup
Test Teardown    Close Application Teardown

*** Test cases ***
Add a recipient for ACH
    [Documentation]    Test that users are able to edit a recipient successfully  
    [Tags]    ACH
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In 
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next
    Enter Move money nav
    Enter Send money to someone page
    Enter Recipient screen
    Click on Edit button
    Edit First name     qa
     :FOR    ${i}    IN RANGE    10
    \    Repeat Keyword    3 times   Swipe ACH screen  
    \    Exit For Loop If    ${i} == 10
    \    Log    ${i}
    Log    Exited
    Hit Save button
    