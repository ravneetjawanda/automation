*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Resource    ../Common/Common_Billpay.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Add a Certified payee for Bill pay
    [Documentation]    Test that users are able to add a Certified Payee successfully  
    [Tags]    Adding_bill_payee
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next 
    Enter Move money nav
    Enter Bill pay page
    Enter Payee screen
    Click on Add Payee button
    Enter or select payee screen
    Enter payee name    Verizon
    Enter zip code    21030
    Click on Search payee button
    Select any payee
    Enter certified account number    123456
    Enter certified confirm account number    123456
    Click on Add payee
    