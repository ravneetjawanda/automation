*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Resource    ../Common/Common_Billpay.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Add a Certified payee for Bill pay
    [Documentation]    Test that users are able to sechdule future dated payments  
    [Tags]    SendingMoney_bill_payee
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In
    Get verified using phone
    Click on Next button
    Sleep    1
    For Test    2
    For Test1   5
    Select Next 
    Enter Move money nav
    Enter Bill pay page
    Enter Payee screen
    Select Certfied payee
    Common_Billpay.Enter Amount    2.00
    Hide Keyboard
    Select Future Date
    Select Submit Date button
    Click Pay Bill button
    Common_Billpay.Click Confirm button
    Common_Billpay.Verify Amount
    Close confirmation screen
    Select Scheduled payments 
    Verify Payee and Amount