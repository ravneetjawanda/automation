*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot
Resource    ../Common/Common_ACH.robot
Resource    ../Common/Common_Goals.robot
Test Setup    Open Application Test Setup

*** Test cases ***
Verify error message is being displayed when user enter amount more than the available balance 
    [Documentation]    Test that users are able to transfer money between goal and account successfully  
    [Tags]    Transfer_money 
     Enter Login Page
     Enter username    rav99@payfare.com
     Enter Password    Payfare@1
     Sign In
     Get verified using phone
     Click on Next button
     Sleep    1
     For Test    2
     For Test1   5
     Select Next
     Click View Goals
     Click Transfer Money button
     Get Balance
     
Transferring Money between Account and Goal
    [Documentation]    Test that users are able to transfer money between account and goal successfully  
    [Tags]    Transfer_money
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare@1
    Sign In
    Get verified using phone
    Click on Next button
    For Test    2
    For Test1   5
    Select Next
    Click View Goals
    Click Transfer Money button
    Enter amount    1.00
    Click Transfer money
    Verify Amount being transferred 
    Click cancel button  
    
Transferring Money between Goals and Account
     [Documentation]    Test that users are able to transfer money between goal and account successfully  
     [Tags]    Transfer_money 
     Enter Login Page
     Enter username    rav99@payfare.com
     Enter Password    Payfare@1
     Sign In
     Get verified using phone
     Click on Next button
     For Test    2
     For Test1   5
     Select Next
     Click View Goals
     Click Transfer Money button
     Click on From dropdown
     Select Goals account
     Enter amount    1.00
     Click Transfer money
     Verify Amount being transferred
     Click cancel button
     

     
     