*** Settings ***
Library    AppiumLibrary   
Library    Collections   

*** Settings ***
Resource    ../Common/Common_Keywords.robot
Resource    ../Common/Common_Login.robot  
Test Setup    Open Application Test Setup 

*** Test Cases ***
My First If condition
    [Documentation]    Test that users are able to transfer money between account and goal successfully  
    [Tags]    Transfer_money
    Enter Login Page
    Enter username    rav99@payfare.com
    Enter Password    Payfare1
    Sign In
    Wait Until Page Contains Element    id=dash_card_checking_balance_amount  
    ${Amount}=    Get Text    id=dash_card_checking_balance_amount 
    Should Be Equal    ${Amount}    $714.90      
   ## Run Keyword If    ${Amount}==$714.90    Log To Console    Test case passes        