*** Settings ***
Library    AppiumLibrary
Library    String     

*** Keywords ***
Click on Drawer menu
    Wait Until Page Contains Element    id=app_top_bar_left_button
    Click Element    id=app_top_bar_left_button    
    
Select Settings
    Wait Until Page Contains Element    xpath=//android.widget.CheckedTextView[@text='Settings']
    Click Element      xpath=//android.widget.CheckedTextView[@text='Settings']
    
Select Edit Profile
    Wait Until Page Contains Element    xpath=//android.widget.TextView[@text='Edit profile']
    Click Element      xpath=//android.widget.TextView[@text='Edit profile']
    
Select Edit email button    
    Wait Until Page Contains Element    id=ll_edit_email    
    Click Element    id=ll_edit_email
    
Enter verification password    [Arguments]    ${Password}
    Wait Until Page Contains Element    id=dialog_password_check_field    
    Click Element    id=dialog_password_check_field
    Input Text    id=dialog_password_check_field    ${Password}
    
Click Submit button
    Click Element    id=dialog_password_check_btn_submit
    
Edit email
    Wait Until Page Contains Element    id=view_profile_email
    Click Element    id=view_profile_email
    Clear Text    id=view_profile_email
    
Enter new email    [Arguments]    ${Email}
    Input Text    id=view_profile_email    ${Email}
    
Select Send code
    Click Element    id=view_profile_email_save_button

Verify Toast message
    Wait Until Page Contains Element    id=snackbar_title
    ${Title}    Get text    id=snackbar_title
    Should Be Equal    ${Title}    Profile updated
    ${Message}    Get Text    id=snackbar_message
    Should be Equal    ${Message}    Be sure to keep your profile up to date.    

##### Mobile number ######    
Select Edit Mobile number button    
    Wait Until Page Contains Element    id=ll_edit_mobile   
    Click Element    id=ll_edit_mobile

Edit phone
    Wait Until Page Contains Element    id=view_profile_phone
    Click Element    id=view_profile_phone
    Clear Text    id=view_profile_phone    
    
Enter new phone    [Arguments]    ${Phone}
    Input Text    id=view_profile_phone    ${Phone}
    
Select Send code Phone button
    Click Element    id=view_profile_phone_save_button
    

  ###### Address #######
Select Edit Address    
    Wait Until Page Contains Element    id=ll_edit_address   
    Click Element    id=ll_edit_address   

Edit Street    [Arguments]    ${Street}
    Wait Until Page Contains Element    id=text_input_et_layout_street_address    
    Click Element    id=text_input_et_layout_street_address
    Clear Text    id=text_input_et_layout_street_address
    Input Text    id=text_input_et_layout_street_address    ${Street}
    
Edit City    [Arguments]    ${City}
    Wait Until Page Contains Element    id=view_profile_addr_city   
    Click Element    id=view_profile_addr_city
    Clear Text    id=view_profile_addr_city
    Input Text    id=view_profile_addr_city    ${City}
    
Enter Zipcode    [Arguments]    ${Zipcode}
    Wait Until Page Contains Element    id=view_profile_addr_zip  
    Click Element    id=view_profile_addr_zip
    Clear Text    id=view_profile_addr_zip
    Input Text    id=view_profile_addr_zip    ${Zipcode}
    
Select State
    Wait Until Page Contains Element    id=view_profile_addr_state_spinner
    Click Element    id=view_profile_addr_state_spinner
    Wait Until Page Contains Element    id=spinner_state
    Click Element    xpath=//android.widget.TextView[@text='California']
    
Select Save button
    Wait Until Page Contains Element    id=view_profile_addr_save_button
    Click Element    id=view_profile_addr_save_button      

Select Address as entered checkbox
    Wait Until Page Contains Element    id=btn_entered_address    
    Click Element    id=btn_entered_address
    
Verify enetered address
    ${EnteredAddress}    Get text    id=text_view_address_entered
    Should Be Equal    ${EnteredAddress}    1451 MONTE DIABLO AVE SAN MATEO, CA 94401
    
Select Suggested address
    Wait Until Page Contains Element    id=btn_suggested_address    
    Click Element    id=btn_entered_address   

Verify Suggested address
    ${SuggestedAddress}    Get Text    id=text_view_address_suggested
    Should Be Equal    ${SuggestedAddress}    1451 MONTE DIABLO AVE SAN MATEO, CA 94401        
 
Click Confirm Address
    Click Element    id=btn_confirm
  

#### Change password #####
Select Change password 
    Wait Until Page Contains Element    xpath=//android.widget.TextView[@text='Change password']
    Click Element      xpath=//android.widget.TextView[@text='Change password'] 
    
Enter New Password    [Arguments]    ${NewPassword}
    Wait Until Page Contains Element    id=view_change_pwd_field2    
    Click Element    id=view_change_pwd_field2
    Input Text    id=view_change_pwd_field2    ${NewPassword}
    
Enter Confirm Password    [Arguments]    ${ConfirmPassword}
    Wait Until Page Contains Element    id=view_change_pwd_field3    
    Click Element    id=view_change_pwd_field3
    Input Text    id=view_change_pwd_field3    ${ConfirmPassword}   
    
Click Save Button
     Click Element    id=view_change_pwd_btn_next
     

### Bank details ####
Select Bank details 
    Wait Until Page Contains Element    xpath=//android.widget.TextView[@text='See bank details']
    Click Element      xpath=//android.widget.TextView[@text='See bank details'] 
    
Verify Account number
    Wait Until Page Contains Element    id=view_profile_bank_info_account    
    ${ActNo}    Get Text    id=view_profile_bank_info_account
    Should Be Equal    ${ActNo}    9898 9800 0021 8972
    
Verify Routing number
    Wait Until Page Contains Element    id=view_profile_bank_routing_account    
    ${RoutingNo}    Get Text    id=view_profile_bank_routing_account
    Should Be Equal    ${RoutingNo}    072 401 06
    
Verify Account Type
    Wait Until Page Contains Element    id=view_profile_bank_account_type    
    ${Account}    Get Text    id=view_profile_bank_account_type
    Should Be Equal    ${Account}    Checking
    
Verify Bank name
    Wait Until Page Contains Element    id=view_profile_bank_name    
    ${Bank}    Get Text    id=view_profile_bank_name
    Should Be Equal    ${Bank}    Stride Bank        

Click Back button
    Click Element    id=tv_back_white
    

      