*** Settings ***
Library    AppiumLibrary

*** Keywords ***
Enter Login Page
    Wait Until Page Contains Element    id=view_welcome_login_button 
    Click Element    id=view_welcome_login_button     
    
Enter Username     [Arguments]    ${username}
    Wait Until Page Contains Element    id=view_login_id_field
    Input Text    view_login_id_field    ${username}
    Set Suite Variable    ${username}
    
Enter Password    [Arguments]    ${password}
    Wait Until Page Contains Element    id=view_login_pwd_field
    Input Password    view_login_pwd_field    ${password}
    Set Suite Variable    ${password}
    
Sign In
    Wait Until Page Contains Element    id=view_login_button
    Click Element    id=view_login_button
    
Check That Login Fails
    Wait Until Page Contains Element    id=view_login_error_messsage
    
Get verified using phone
    Wait Until Page Contains Element    id=radio_phone
    Click Element    id=radio_phone
    
Get verified using email
    Wait Until Page Contains Element    id=radio_email
    Click Element    id=radio_email       
    
Click on Next button
    Click Element    id=btn_next_login_verification_selection_method
    
For Test    [Arguments]    ${2fa}
    :FOR    ${i}    IN RANGE    3
    \    Input text        xpath=//android.widget.EditText[@index='${i}']    ${2fa}  
    \    Exit For Loop If    ${i} == 3
    \    Log    ${i}
    Log    Exited   
    
For Test1    [Arguments]    ${2fa1}
    :FOR    ${j}    IN RANGE    3    6
    \    Input text        xpath=//android.widget.EditText[@index='${j}']    ${2fa1}  
    \    Exit For Loop If    ${j} == 6
    \    Log    ${j}
    Log    Exited 
  
Select Remember me
    Click Element    id=checkbox_remember_device  
 
Select Next
    Click Element    id=next_button   
    
Verification required Error
    Wait Until Page Contains Element    id=dialog_btn_multistage_login_ok
    Click Element    id=dialog_btn_multistage_login_ok  
    