*** Settings ***
Library    AppiumLibrary
Library    String     

*** Keywords ***
Click View Goals
    Wait Until Page Contains Element    id=dash_card_savings_add_account_button
    Click Element    id=dash_card_savings_add_account_button
    
Click Transfer Money button 
    Wait Until Page Contains Element    id=view_transfer_money_button
    Click Element    id=view_transfer_money_button
    
Enter amount    [Arguments]    ${Amount}
    Wait Until Page Contains Element    id=edit_text_amount
    Input text    id=edit_text_amount    ${Amount}
    
Click Transfer money 
    Wait Until Page Contains Element    id=button_transfer_money
    Click Element    id=button_transfer_money
    
Verify Amount being transferred
    Wait Until Page Contains Element    id=view_ach_transfer_amount    
    ${Amount1}=    Get Text    id=view_ach_transfer_amount 
    Should Be Equal    ${Amount1}    $1.00
    
Click cancel button
    Wait Until Page Contains Element    id=view_ach_transfer_confirmation_close_button    
    Click Element    id=view_ach_transfer_confirmation_close_button
    

#### Tranfer from Goals to Account
Click on From dropdown
    Wait Until Page Contains Element    id=tv_title_from
    Click Element    id=tv_title_from
    
Select Goals account
    Wait Until Element Is Visible    xpath=//android.widget.TextView[@text='Goal']    
    Click Element    xpath=//android.widget.TextView[@text='Goal'] 
    
### Verify error message 
Get Balance 
    Wait Until Page Contains Element    id=tv_balance_from    
    ${AccountBal}=    Get Text    id=tv_balance_from 
    ${Amount}=    Fetch From Right    ${AccountBal}    $
    ${Amount}=    Remove String    ${Amount}    ,
    ${IncAmount}=    Evaluate    ${Amount} + 100.00  
    ${Amt}=    Convert To String    ${IncAmount}
    Wait Until Page Contains Element    id=edit_text_amount
    Input text    id=edit_text_amount    ${Amt} 
    ${status}=    Get Text    id=textinput_error
    Run Keyword If    ‘Available to send - $${Amount}’=='${status}'    Pass Execution    Error message matches        
    