*** Settings ***
Library    AppiumLibrary

*** Variables ***
${APPIUM_SERVER1}    http://0.0.0.0:4723/wd/hub
${platform_Name}    android
${platform_Version}    8.0
${device_Name}    Pixel 2 XL
${udid}    807KPZK2008797
${app_Package}    com.payfare.lyft
${app_Activity}    com.payfare.lyft.ui.authentication.WelcomeScreenActivity


*** Keywords *** 
Open Application Test Setup    
    Open Application    ${APPIUM_SERVER1}    platformName=${platform_Name}    platformVersion=${platform_Version}        deviceName=${device_Name}    udid=${udid}    appPackage=${app_Package}    appActivity=${app_Activity}

Close Application Teardown
    Close Application