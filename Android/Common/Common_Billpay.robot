*** Settings ***
Library    AppiumLibrary
Library    DateTime
Library    String

*** Keywords ***
Enter Bill pay page 
    Wait Until Page Contains Element    id=view_money_money_card_pay_title
    Click Element    id=view_money_money_card_pay_title
 
Enter Payee screen
    Wait Until Page Contains Element    id=view_bill_payment_payee
    Click Element    id=view_bill_payment_payee
    
Click on Add Payee button
    Wait Until Page Contains Element    id=view_select_bill_payee_add_button   
    Click Element    id=view_select_bill_payee_add_button
    
Enter or select payee screen
    Click Element    id=view_bill_payee_name
    
Enter payee name    [Arguments]    ${payeeName} 
    Wait Until Page Contains Element    id=view_search_bill_payee_name
    Input text    id=view_search_bill_payee_name    ${payeeName}
    
Enter zip code    [Arguments]    ${zipCode}
    Wait Until Page Contains Element    id=view_search_bill_payee_zip    
    Input text    id=view_search_bill_payee_zip    ${zipCode}
    
Click on Search payee button
    Wait Until Page Contains Element    id=view_search_bill_payee_manual_search_button 
    Click Element    id=view_search_bill_payee_manual_search_button
    
Select any payee 
    Wait Until Page Contains Element    //android.widget.TextView[@text='VERIZON - PO BOX 60']    
    Click Element    //android.widget.TextView[@text='VERIZON - PO BOX 60']
    
Enter Add payee screen
    Wait Until Page Contains Element    id=view_select_ach_recipient_add    
    Click Element    id=view_select_ach_recipient_add
    
Enter certified account number    [Arguments]    ${payeeAccountNumber} 
    Input text    id=view_bill_payee_account_no    ${payeeAccountNumber}
    
Enter certified confirm account number    [Arguments]    ${payeeConfirmAccountNumber} 
    Input text    id=view_bill_payee_account_confirmation    ${payeeConfirmAccountNumber}
    
Click on Add payee
    Click Element    id=view_bill_payee_save_button
    
Confirm payee got added
    Wait Until Page Contains Element    id=view_select_ach_recipient_add 
    
For error message 
    Wait Until Page Contains Element    id=view_login_error_messsage
    

### Manually Add a payee
Select Manually Add payee button
    Click Element    id=view_search_bill_payee_manual_add_button
    
Enter manual payee name    [Arguments]    ${payeeName} 
    Wait Until Page Contains Element    id=view_bill_payee_name    
    Input text    id=view_bill_payee_name    ${payeeName}
    
Enter address    [Arguments]    ${payeeAddress} 
    Input text    id=view_bill_payee_address    ${payeeAddress}
    
Enter city   [Arguments]    ${payeeCity} 
    Input text    id=view_bill_payee_city   ${payeeCity}
    
Select state
    Click Element    id=view_bill_payee_state_spinner
    Wait Until Page Contains Element    //android.widget.TextView[@text='California']    
    Click Element    //android.widget.TextView[@text='California']

Enter manual payee zipcode   [Arguments]    ${payeeZipCode} 
    Input text    id=view_bill_payee_zip    ${payeeZipCode}
    
Click Confirm address button
    Click Element    id=btn_confirm
   
Select Save button
    Click Element    id=view_bill_payee_save_button
  
## Edit certified payee
Click Edit Button
    Wait Until Page Contains    //android.widget.ImageButton[@index=‘1’]
    Click Element    //android.widget.ImageButton[@index=‘1’] 
           
Select Edit button
    Click Element    id=item_bill_payee_icon_edit_button
    
Edit Payee Name    [Arguments]    ${editPayeeName}
    Input text    id=view_bill_payee_name    ${editPayeeName}
    
## Delete a certified payee
Click Delete Payee
    Wait Until Page Contains Element    id=view_bill_payee_delete_button
    Click Element    id=view_bill_payee_delete_button    

##Send Money to certified payee 
Select Certfied payee
    Wait Until Page Contains Element    //android.widget.TextView[@text='VERIZON']   
    Click Element    //android.widget.TextView[@text='VERIZON']    
    
Enter Amount    [Arguments]    ${Amount}
    Wait Until Page Contains Element    id=view_bill_payment_amount
    Click Element    id=view_bill_payment_amount
    Input Text    id=view_bill_payment_amount    ${Amount}
    
Select Todays Date
     Wait Until Page Contains Element    id=view_bill_payment_send_on
     Click Element    id=view_bill_payment_send_on
     ${Date}    Get Current Date    result_format=%d
     Run Keyword If    '${Date}' < '10'    Remove String    ${Date}    0   
     Click Element    //android.view.View[@text='${Date}']  
     Run Keyword If    '${Date}' > '10'    Click Element    //android.view.View[@text='${Date}']  

Select Future Date
     Wait Until Page Contains Element    id=view_bill_payment_send_on
     Click Element    id=view_bill_payment_send_on
     ${Date}    Get Current Date    result_format=%Y-%m-%d
     ${IncrementedDate} =  Add Time To Date  ${Date}  2 days
     ${Scheduleddate}    Convert Date    ${IncrementedDate}    result_format=%d
     Run Keyword If    '${Scheduleddate}' < '10'    Remove String    ${Scheduleddate}    0
     Click Element    //android.view.View[@text='${Scheduleddate}']  
     Run Keyword If    '${Scheduleddate}' > '10'    Click Element    //android.view.View[@text='${Scheduleddate}']    

Select Submit Date button
    Wait Until Page Contains Element    id=date_picker_button
    Click Element    id=date_picker_button      
    
Select Dismiss button
     Wait Until Page Contains Element    id=date_picker_button_cancel
     Click Element    id=date_picker_button_cancel

Click Pay Bill button
     Wait Until Page Contains Element    id=view_bill_payment_button  
     Click Element    id=view_bill_payment_button  
     
Click Confirm button
     Wait Until Page Contains Element    id=view_snackbar_confirm_button   
     Click Element    id=view_snackbar_confirm_button       

Verify Amount
     Wait Until Page Contains Element    id=view_bill_payment_amount
     ${Amt}    Get Text    id=view_bill_payment_amount  
     Run Keyword If    '${Amt}' == '$2.00'    Pass Execution    Amount matches 
     
Close confirmation screen
     Wait Until Page Contains Element    id=view_bill_payment_confirmation_close_button  
     Click Element    id=view_bill_payment_confirmation_close_button
     
### Check Scheduled payments 
Select Scheduled payments 
     Wait Until Page Contains Element    id=view_money_money_card_pay_scheduled_button  
     Click Element    id=view_money_money_card_pay_scheduled_button
     
Verify Payee and Amount
     Wait Until Page Contains Element    id=item_bill_payment_amount
     ${Amt}    Get Text    id=item_bill_payment_amount  
     Run Keyword If    '${Amt}' == '$2.00'    Pass Execution    Amount matches  
     
Click Edit Button
     Wait Until Page Contains Element    id=item_bill_payment_edit_btn  
     Click Element    id=item_bill_payment_edit_btn
     
Edit Amount    [Arguments]    ${NewAmount}
     Wait Until Page Contains Element    id=view_edit_bill_amount  
     Click Element    id=view_edit_bill_amount
     Input Text    id=view_edit_bill_amount    ${NewAmount}

Edit Date
     Click Element    id=view_edit_bill_send_on
     ${Date}    Get Current Date    result_format=%Y-%m-%d
     ${IncrementedDate} =  Add Time To Date  ${Date}  5 days
     ${Scheduleddate}    Convert Date    ${IncrementedDate}    result_format=%d
     Run Keyword If    '${Scheduleddate}' < '10'    Remove String    ${Scheduleddate}    0
     Click Element    //android.view.View[@text='${Scheduleddate}']  
     Run Keyword If    '${Scheduleddate}' > '10'    Click Element    //android.view.View[@text='${Scheduleddate}']
    
Click Save button
     Wait Until Page Contains Element    id=view_edit_bill_save_button  
     Click Element    id=view_edit_bill_save_button

Click Cancel Payment button
     Wait Until Page Contains Element    id=view_edit_bill_cancel_button  
     Click Element    id=view_edit_bill_cancel_button
     


