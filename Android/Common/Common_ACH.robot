*** Settings ***
Library    AppiumLibrary

*** Keywords ***
Enter Move money nav
    Wait Until Page Contains Element    id=bottom_nav_move_money    
    Click Element    id=bottom_nav_move_money

Enter Send money to someone page 
    Wait Until Page Contains Element    id=view_money_money_card_send
    Click Element    id=view_money_money_card_send
    
Enter Recipient screen
    Wait Until Page Contains Element    id=view_send_money_recipient
    Click Element    id=view_send_money_recipient
    
Click on Add button
    Wait Until Page Contains Element    id=view_select_ach_recipient_add    
    Click Element    id=view_select_ach_recipient_add
    
Enter FirstName    [Arguments]    ${firstName}
    Wait Until Page Contains Element    id=view_add_recipient_first_name
    Input text    id=view_add_recipient_first_name    ${firstName} 

Enter LastName    [Arguments]    ${firstName}  
   # Wait Until Page Contains Element    id=view_add_recipient_last_name  
    Input text    id=view_add_recipient_last_name    ${firstName}
    
Enter Mobile number    [Arguments]    ${mobileNumber} 
   # Wait Until Page Contains Element    id=view_add_recipient_mobile   
    Input text    id=view_add_recipient_mobile   ${mobileNumber}
    
Enter bank name    [Arguments]    ${bankName}
  #  Wait Until Page Contains Element    id=view_add_recipient_card_bank_name
    Input text    id=view_add_recipient_card_bank_name    ${bankName}
    
Enter account number    [Arguments]    ${accountNumber}
  #  Wait Until Page Contains Element    id=view_add_recipient_card_bank_account_number
    Input text    id=view_add_recipient_card_bank_account_number    ${accountNumber}
    
Enter confirm account number    [Arguments]    ${confirmAccountNumber}
    Wait Until Page Contains Element    id=view_add_recipient_card_bank_account_number_confirm    
    Input text    id=view_add_recipient_card_bank_account_number_confirm   ${confirmAccountNumber}
    
Select Account Type
    Wait Until Page Contains Element    id=view_send_money_account_type_state_spinner
    Click Element    id=view_send_money_account_type_state_spinner
    Wait Until Page Contains Element    id=spinner_item_text_view
    Click Element    xpath=//android.widget.TextView[@text='Savings']
    
Swipe Up
  ${element_size}=    Get Element Size    id=view_add_recipient_card_bank_account_number
  ${element_location}=    Get Element Location    id=view_add_recipient_card_bank_account_number
  ${start_x}=         Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
  ${start_y}=         Evaluate      ${element_location['y']} + (${element_size['height']} * 0.7)
  ${end_x}=           Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
  ${end_y}=           Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
  Swipe               ${start_x}    ${start_y}  ${end_x}  ${end_y}  1000
  Sleep  1
    
Enter routing number    [Arguments]    ${routingNumber}
    Input text    id=view_add_recipient_card_bank_routing_number    ${routingNumber}
    
Click on Save button
    Click Element    id=view_add_recipient_save_button
    

### Edit Recipient
Click on Edit button
    Wait Until Page Contains Element    item_ach_recipient_img    
    Click Element    item_ach_recipient_img
    
Edit First name    [Arguments]    ${firstName} 
    Wait Until Page Contains Element    id=view_edit_recipient_first_name
    Clear Text    id=view_edit_recipient_first_name
    Input text    id=view_edit_recipient_first_name    ${firstName} 
    
Swipe ACH screen
  Wait Until Page Contains Element    id=view_edit_recipient_card_bank_account_number
  ${element_size}=    Get Element Size    id=view_edit_recipient_card_bank_account_number
  ${element_location}=    Get Element Location    id=view_edit_recipient_card_bank_account_number
  ${start_x}=         Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
  ${start_y}=         Evaluate      ${element_location['y']} + (${element_size['height']} * 0.7)
  ${end_x}=           Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
  ${end_y}=           Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
  Swipe               ${start_x}    ${start_y}  ${end_x}  ${end_y}  1000
  Sleep  1
    
Hit Save button
    Click Element    id=view_edit_recipient_save_button
    
### Delete Recipient 
Hit Delete button
     Wait Until Page Contains Element    id=view_edit_recipient_delete_button
     Click Element    id=view_edit_recipient_delete_button
    
### Send Money to a recipient
Select Recipient
    Wait Until Page Contains Element    id=item_ach_recipient_name
    Click Element    id=item_ach_recipient_name
    
Enter Amount    [Arguments]    ${Amount}
    Wait Until Page Contains Element    id=view_send_money_amount
    Click Element    id=view_send_money_amount
    Input Text    id=view_send_money_amount    ${Amount}
    
Enter Optional Message    [Arguments]    ${Msg}
    Wait Until Page Contains Element    id=view_send_money_message
    Click Element    id=view_send_money_message
    Input Text    id=view_send_money_message    ${Msg}
    
Click Send Money button
    Click Element    id=view_send_money_button
    
Click Confirm button
    Wait Until Page Contains Element    id=view_snackbar_confirm_button
    Click Element    id=view_snackbar_confirm_button
    
Verify Amount
     Wait Until Page Contains Element    id=view_ach_transfer_amount
     ${Amt}    Get Text    id=view_ach_transfer_amount  
     Run Keyword If    ‘${Amt}’ == '$2.00'    Pass Execution    Amount matches  
    
    
    