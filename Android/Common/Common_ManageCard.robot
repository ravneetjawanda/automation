*** Settings ***
Library    AppiumLibrary
Library    String  

*** Variables ***
${LockedTitle}    Card locked
${UnlockedTitle}    Card unlocked   

*** Keywords *** 
### Lock/Unlock Cards
Select Manage Card
    Wait Until Page Contains Element    xpath=//android.widget.CheckedTextView[@text='Manage card']
    Click Element      xpath=//android.widget.CheckedTextView[@text='Manage card']
    
Select Card status
    Wait Until Page Contains Element    id=view_card_management_activate_title
    Click Element      id=view_card_management_activate_title
    
Click Freeze/Unfreeze button
    Wait Until Page Contains Element    id=tv_card_status_value    
    Click Element    id=view_freeze_card
    
Verify Status
    ${CardStatus}    Get Text    id=tv_card_status_value
    Run Keyword If    '${CardStatus}' == 'Active'    
    ...    Log    Card is in Active state    
    ...    ELSE IF    '${CardStatus}' == 'Locked'    
    ...    Log    Card is Locked        
  
Verify Snack Bar
    Wait Until Page Contains Element    id=snackbar_no_title_message
    ${Title}    Get text    id=snackbar_no_title_message
    Run Keyword If    '${Title}' == '${LockedTitle}'    
    ...    Log     Card locked 
    ...    ELSE IF    '${Title}' == '${UnlockedTitle}'
    ...    Log    Card unlocked         

### Report a card as lost or stolen 

Select Report a card issue
    Wait Until Page Contains Element    id=view_card_management_report_stolen_card_title
    Click Element      id=view_card_management_report_stolen_card_title
    
Select Reason
    Wait Until Page Contains Element    id=view_card_issue_reason_spinner
    Click Element      id=view_card_issue_reason_spinner
    Wait Until Page Contains Element    id=spinner_reason
    Click Element    xpath=//android.widget.TextView[@text='Lost']
    
Enter Description    [Arguments]    ${Description}
     Wait Until Page Contains Element    id=view_card_issue_description
     Click Element      id=view_card_issue_description
     Input Text    id=view_card_issue_description    ${Description}  
     
Select Next button
     Wait Until Page Contains Element    id=view_card_issue_next_button
     Click Element      id=view_card_issue_next_button
     
Edit Street    [Arguments]    ${Street}
    Wait Until Page Contains Element    id=input_layout_et_card_issue_street_address    
    Click Element    id=input_layout_et_card_issue_street_address
    Clear Text    id=input_layout_et_card_issue_street_address
    Input Text    id=input_layout_et_card_issue_street_address    ${Street}
 
Enter Apartment/Unit    [Arguments]    ${Apt}
    Wait Until Page Contains Element    id=input_layout_et_card_issue_apartment_address    
    Click Element    id=input_layout_et_card_issue_apartment_address
    Clear Text    id=input_layout_et_card_issue_apartment_address
    Input Text    id=input_layout_et_card_issue_apartment_address    ${Apt}
       
Edit City    [Arguments]    ${City}
    Wait Until Page Contains Element    id=view_card_issue_addr_city   
    Click Element    id=view_card_issue_addr_city
    Clear Text    id=view_card_issue_addr_city
    Input Text    id=view_card_issue_addr_city    ${City}
    
Enter Zipcode    [Arguments]    ${Zipcode}
    Wait Until Page Contains Element    id=view_card_issue_addr_zip  
    Click Element    id=view_card_issue_addr_zip
    Clear Text    id=view_card_issue_addr_zip
    Input Text    id=view_card_issue_addr_zip    ${Zipcode}
    
Select State
    Wait Until Page Contains Element    id=spinner_addr_state_spinner
    Click Element    id=spinner_addr_state_spinner
    Wait Until Page Contains Element    id=spinner_state
    Click Element    xpath=//android.widget.TextView[@text='California']
    
Click Next
    Wait Until Page Contains Element    id=view_card_issue_addr_next_button
    Click Element      id=view_card_issue_addr_next_button
 
Verify Toast message
    Wait Until Page Contains Element    id=snackbar_title
    ${Title}    Get text    id=snackbar_title
    Should Be Equal    ${Title}    Your new card is on its way
    Wait Until Page Contains Element    id=snackbar_message
    ${Message}    Get text    id=snackbar_message
    Should Be Equal    ${Message}    We locked your old card, but you can still pay bills and transfer money in the app.    

#### Change Card PIN

Select Change PIN
    Wait Until Page Contains Element    id=view_card_management_change_pin_title
    Click Element      id=view_card_management_change_pin_title
    
Enter PIN    [Arguments]    ${PIN}
    Wait Until Page Contains Element    id=view_card_change_pin_form_field_pin    
    Click Element    id=view_card_change_pin_form_field_pin
    Input Text    id=view_card_change_pin_form_field_pin    ${PIN}
    
Confirm PIN    [Arguments]    ${ConfirmPIN}
    Wait Until Page Contains Element    id=view_card_change_pin_form_field_pin_confirm    
    Click Element    id=view_card_change_pin_form_field_pin_confirm
    Input Text    id=view_card_change_pin_form_field_pin_confirm    ${ConfirmPIN}
    
Hit Next button
     Wait Until Page Contains Element    id=view_card_change_pin_button_save
     Click Element      id=view_card_change_pin_button_save
     
Verify confirmation message
     Wait Until Page Contains Element    id=snackbar_title
     ${Title}    Get text    id=snackbar_title
     Should Be Equal    ${Title}    You card PIN has been updated!
     Wait Until Page Contains Element    id=snackbar_message
     ${Message}    Get text    id=snackbar_message
     Should Be Equal    ${Message}    Make sure to keep it safe.
        

     
    
